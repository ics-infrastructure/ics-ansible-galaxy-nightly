import time
import click
import requests
from collections import namedtuple

Pipeline = namedtuple('Pipeline', 'project_id, project_name, pipeline_id')


class Gitlab:

    def __init__(self, token, url='https://gitlab.esss.lu.se'):
        self.token = token
        self.api_url = url + '/api/v4'
        self.session = requests.Session()
        self.session.headers.update({'Private-Token': token})

    def request(self, method, endpoint, **kwargs):
        r = self.session.request(method, self.api_url + endpoint, **kwargs)
        r.raise_for_status()
        return r

    def get(self, endpoint, **kwargs):
        return self.request('GET', endpoint, **kwargs)

    def post(self, endpoint, **kwargs):
        return self.request('POST', endpoint, **kwargs)

    def put(self, endpoint, **kwargs):
        return self.request('PUT', endpoint, **kwargs)

    def get_all(self, endpoint, **kwargs):
        """Generator that returns all items from the endpoint"""
        r = self.get(endpoint, **kwargs)
        for item in r.json():
            yield item
        while 'next' in r.links:
            url = r.links['next']['url']
            r = self.session.get(url)
            r.raise_for_status()
            for item in r.json():
                yield item

    def enable_shared_runners(self, project):
        """Enable shared runners for the project"""
        r = self.put(f'/projects/{project["id"]}', params={"shared_runners_enabled": True})
        return r.json()

    def get_group_projects(self, group):
        """Return all projects part of this group"""
        for project in self.get_all(f'/groups/{group}/projects'):
            yield project

    def create_project_trigger(self, project, trigger_description):
        """Create and return a project trigger"""
        print(f'Create trigger {trigger_description} for project {project["name"]}')
        r = self.post(f'/projects/{project["id"]}/triggers',
                      data={'description': trigger_description})
        return r.json()

    def get_project_trigger(self, project, trigger_description):
        """Return the project trigger based on description"""
        for trigger in self.get_all(f'/projects/{project["id"]}/triggers'):
            if trigger['description'] == trigger_description:
                return trigger
        return None

    def trigger_pipeline(self, project, trigger_description, ref='master'):
        """Trigger the project pipeline"""
        # To trigger a pipeline via the API, a trigger token is required.
        # The trigger can be retrieved or created via the API.
        trigger = self.get_project_trigger(project, trigger_description)
        if trigger is None:
            trigger = self.create_project_trigger(project, trigger_description)
        print(f'Trigger pipeline {trigger_description} for project {project["name"]}')
        try:
            # If the project doesn't include a .gitlab-ci.yml, this will raise a 400
            r = self.post(f'/projects/{project["id"]}/trigger/pipeline',
                          data={'token': trigger['token'], 'ref': ref})
        except requests.HTTPError as e:
            print(e)
            print('No .gitlab-ci.yml...')
            return None
        else:
            return Pipeline(project['id'], project['name'], r.json()['id'])

    def get_pipeline_status(self, project_id, pipeline_id):
        r = self.get(f'/projects/{project_id}/pipelines/{pipeline_id}')
        return r.json()['status']

    def wait_for_pipeline(self, project_id, pipeline_id):
        status = self.get_pipeline_status(project_id, pipeline_id)
        while status in ('pending', 'running'):
            # print(f'Status for pipeline {pipeline_id}: {status}. Waiting...')
            time.sleep(30)
            status = self.get_pipeline_status(project_id, pipeline_id)
        return status


@click.command()
@click.option('--gitlab-token', help='GitLab API token')
@click.option('--trigger-name', help='Trigger name [default: nightly-test]',
              default='nightly-test')
def cli(gitlab_token, trigger_name):
    gitlab = Gitlab(gitlab_token)
    # Trigger all pipelines
    pipelines = [gitlab.trigger_pipeline(project, trigger_name)
                 for project in gitlab.get_group_projects('ics-ansible-galaxy')]
    print('Wait for all pipelines to complete...')
    for pipeline in pipelines:
        if pipeline is not None:
            status = gitlab.wait_for_pipeline(pipeline.project_id, pipeline.pipeline_id)
            print(f'Status for {pipeline.project_name} pipeline {pipeline.pipeline_id}: {status}')


if __name__ == '__main__':
    cli()
