ics-ansible-galaxy-nightly
==========================

This repository defines a gitlab-ci pipeline to test all the Ansible roles and playbooks
part of the ics-ansible-galaxy group on gitlab.esss.lu.se.
